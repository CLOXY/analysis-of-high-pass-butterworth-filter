function Hd = HighPassFilter(Fc, Fs, N)
% Returns a discrete-time filter object.
% Butterworth Highpass filter designed using FDESIGN.HIGHPASS.
% All frequency values are in Hz.

% Construct an FDESIGN object and call its BUTTER method.
h  = fdesign.highpass('N,F3dB', N, Fc, Fs);
Hd = design(h, 'butter');

% Reading the info from source files.
[tm,signal,Fs,siginfo]=rdmat('b001m');

% Adding the low frequency inteferance.
step = 1/Fs;
last = length(signal)/Fs;
t = 0 : step : last - step;
x = cos(2*pi*0.01*t)';
motionSignal = signal + x;

% Showing the signals.
figure();
subplot(2, 1, 1); plot(signal);
title('Source signal');
subplot(2, 1, 2); plot(motionSignal);
title('Motion signal');


Fc = 0.05;
N = 3;
while(Fc <= 0.5)
    % Filtration.
    Hd = HighPassFilter(Fc, Fs, N);
    Signal = filter(Hd,motionSignal);    
    
    % Showing the fitred and unfiltred signals.
    figure();
    plot(Signal, 'red'); 
    hold on;
    plot(signal, 'green'); 
    Fc = Fc + 0.05;
end